// import React, { Component } from 'react';
// import {createAppContainer} from 'react-navigation';
// import {createStackNavigator} from 'react-navigation-stack';
// import  { HomeScreen, ProfileScreen } from './app/home/home';

// const MainNavigator = createStackNavigator({
//   Home: {screen: HomeScreen},
//   Profile: {screen: ProfileScreen},
// });

// const App = createAppContainer(MainNavigator);

// export default App;
import React, { Component } from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import  { HomeScreen, ProfileScreen } from './app/home/home';
import  { LoginScreen } from './app/login/login';
import  { RegisterScreen } from './app/login/register';

const MainNavigator = createStackNavigator({
  Login : {screen: LoginScreen},
  Register : {screen: RegisterScreen},
  Home: {screen: HomeScreen},
  // Profile: {screen: ProfileScreen},
});

const App = createAppContainer(MainNavigator);

export default App;

