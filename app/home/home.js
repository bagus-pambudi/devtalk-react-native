import React, { Component } from 'react';
import { Button } from 'react-native';

export class HomeScreen extends Component {
    static navigationOptions = {
      title: 'Welcome',
    };
    render() {
      const {navigate} = this.props.navigation;
      return (
        <Button
          title="Go to Jane's profile"
          onPress={() => navigate('Profile', {name: 'Jane'})}
        />
      );
    }
  }
export class ProfileScreen extends Component {
    static navigationOptions = {
      title: 'Profile',
    };
    render() {
      const { navigate } = this.props.navigation;
      return (
        <Button
          title="Back To the Home"
          onPress={() =>
            navigate('Home', { name: 'Home' })
          }
        />
      );
    }
}