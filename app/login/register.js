import React, { Component } from "react";
import {
  View,
  TextInput,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";


export class RegisterScreen extends Component {
constructor(props) {
    super(props);
    this.state = {
        fullname: '',   
        username: '',   
        password: '',   
        email: '',   
        handphone: ''
    };
};
static navigationOptions = ({ navigation }) => ({
    header : null,
})
render(){
  const {navigate} = this.props.navigation;
  return (
    <KeyboardAvoidingView behavior="height" style={{ flex:1 }} enabled>
      <ImageBackground  style={ styles.container }
      source={require('../../assets/bg1.jpg')}>
        <TextInput
          style={styles.input}
          onChangeText={text =>  this.setState({fullname}) }
          placeholder="Full Name"
        />
        <TextInput
          style={styles.input}
          onChangeText={text =>  this.setState({username})}
          placeholder="Username"
        />
        <TextInput
          style={styles.input}
          onChangeText={text =>  this.setState({email})}
          placeholder="Email"
          textContentType= "emailAddress"
        />
        <TextInput
          style={styles.input}
          onChangeText={text =>  this.setState({handphone})}
          placeholder="Handphone Number : 62xxxxxxxxx"
          textContentType = "telephoneNumber"
        />
        <TextInput
          style={styles.input}
          onChangeText={text =>  this.setState({password})}
          placeholder="Password"
          secureTextEntry={true}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            if(this.setState.username == null || this.setState.password == null){
              alert("Maaf username atau password tidak boleh kosong");             
            }else{
              if(this.setState.username == "bagus"){
                if(password == "ganteng"){
                  
                }else{
                  alert("Maaf password Anda salah");
                }
              }else{
                alert("Maaf username Anda tidak terdaftar");
              }
            }
            console.log( "Your username is " + username + ", and your password is " + password);
          }}>
          <Text style={styles.buttonText}>REGISTER</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.signupText}>Already have an account yet? </Text>
          <TouchableOpacity style={{ marginTop:15 }} onPress={() => navigate('Login')}>
            <Text style={{ fontWeight: "bold", textDecorationLine: 'underline' }}>Login</Text>
          </TouchableOpacity>
        </View>
        
      </ImageBackground>
    </KeyboardAvoidingView>
  )
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 32,
    marginBottom: 86
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft:40,
    paddingRight: 40
  },
  input: {
    backgroundColor: "#eeeeee",
    width: "100%",
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 20,
    fontSize: 16,
    borderRadius: 50
  },
  button: {
    paddingTop: 15,
    paddingBottom: 15,
    width: "100%",
    backgroundColor: "#7C36C4",
    borderRadius: 50
  },
  buttonText: {
    color: "white",
    fontSize: 16,
    textAlign: "center",
    fontWeight: "bold"
  },
  signupText:{
    color: 'white',
    marginTop: 15,
    fontSize: 16
  }
});