import React, { Component } from "react";
import {
  View,
  TextInput,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Switch
} from "react-native";

export class LoginScreen extends Component {

state = {
  username: '',
  password: '',
  hidden: true
}

onInputLabelPressed = () => {
  this.setState({ hidden: !this.state.hidden });
}

handleUsername = (text) => {
  this.setState({ username: text })
}
handlePassword = (text) => {
  this.setState({ password: text })
}
static navigationOptions = ({ navigation }) => ({
    header : null,
})
render(){
  const {navigate} = this.props.navigation;
  return (
    <KeyboardAvoidingView behavior="padding" style={{ flex:1 }}>
      <ImageBackground  style={ styles.container }
      source={require('../../assets/bg1.jpg')}>
        <Image
          source={require('../../assets/ic_launcher.png')}
          style={{ width: 140, height: 140, marginBottom: 10 }}
        />
        <TextInput
          style={styles.input}
          onChangeText={this.handleUsername}
          placeholder="Username"
        />
        <View style={styles.input2}>
          <TextInput
            style={{width:"90%"}}
            onChangeText={this.handlePassword}
            placeholder="Password"
            secureTextEntry={this.state.hidden}
          />
          <TouchableOpacity onPress={this.onInputLabelPressed}>
          {this.state.hidden ? 
            <Image
            source={{ uri: "https://cdn0.iconfinder.com/data/icons/zondicons/20/view-show-256.png" }}
            style={{ width: 25, height: 20, marginTop:5 }} /> : 
            <Image
            source={{ uri: "https://cdn1.iconfinder.com/data/icons/material-core/22/visibility-off-512.png" }}
            style={{ width: 25, height: 20, marginTop:5 }} />
          }
          </TouchableOpacity>
         
        </View>
        
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            if(this.state.username == null || this.state.password == null){
              alert("Maaf username atau password tidak boleh kosong");             
            }else{
              if(this.state.username == "bagus"){
                if(this.state.password == "ganteng"){
                  navigate('Home', {name: 'Home'})
                }else{
                  alert("Maaf password Anda salah");
                }
              }else{
                alert("Maaf username Anda tidak terdaftar");
              }
            }
            console.log( "Your username is " + this.state.username + ", and your password is " + this.state.password);
          }}>
          <Text style={styles.buttonText}>SIGN IN</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.signupText}>Don't have an account yet? </Text>
          <TouchableOpacity style={{ marginTop:15 }} onPress={() => navigate('Register', {name: 'Jane'})}>
            <Text style={{ fontWeight: "bold", textDecorationLine: 'underline' }}>Signup</Text>
          </TouchableOpacity>
        </View>
        
      </ImageBackground>
    </KeyboardAvoidingView>
  );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 32,
    marginBottom: 86
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft:40,
    paddingRight: 40
  },
  input: {
    backgroundColor: "#eeeeee",
    width: "100%",
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 20,
    fontSize: 16,
    borderRadius: 50
  },
  input2: {
    backgroundColor: "#eeeeee",
    width: "100%",
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 20,
    fontSize: 16,
    borderRadius: 50,
    flexDirection: "row"
  },
  button: {
    paddingTop: 15,
    paddingBottom: 15,
    width: "100%",
    backgroundColor: "#7C36C4",
    borderRadius: 50
  },
  buttonText: {
    color: "white",
    fontSize: 16,
    textAlign: "center",
    fontWeight: "bold"
  },
  signupText:{
    color: 'white',
    marginTop: 15,
    fontSize: 16
  }
});